const assert = require('assert');
const addUserToGolfclub = require('../../src/hooks/add-user-to-golfclub');

describe('\'add user to golfclub\' hook', () => {
  it('runs the hook', () => {
    // A mock hook object
    const mock = {};
    // Initialize our hook with no options
    const hook = addUserToGolfclub();

    // Run the hook function (which returns a promise)
    // and compare the resulting hook object
    return hook(mock).then(result => {
      assert.equal(result, mock, 'Returns the expected hook object');
    });
  });
});
