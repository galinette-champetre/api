const assert = require('assert');
const app = require('../../src/app');

describe('\'pub\' service', () => {
  it('registered the service', () => {
    const service = app.service('pub');

    assert.ok(service, 'Registered the service');
  });
});
