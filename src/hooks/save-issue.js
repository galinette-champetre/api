// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return async function saveIssue(hook) {
    const { golfId } = hook.data;
    const { _id } = hook.result

    hook.data = await hook.app.service('golf-clubs').get(golfId);

    const newIssueArray = [...hook.data.issueIds, _id]
    await hook.app.service('golf-clubs').patch(golfId, {issueIds: newIssueArray} )

    return hook;
  };
}
