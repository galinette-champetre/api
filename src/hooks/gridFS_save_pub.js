// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
const Duplex = require('stream').Duplex; 

module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
    return async function gridFSSavePub(hook) {
        const gfs = hook.app.get('gfs');
        const file = hook.params.file;
        const metadata = hook.data

        console.log(hook.data)

        let stream = new Duplex();
        stream.push(file.buffer);
        stream.push(null);

        // delete previous file
        await gfs.remove({ filename: 'pub.jpg' })
        await hook.app.service('pub').remove(null)

        const writestream = await gfs.createWriteStream({
            filename: 'pub.jpg',
            content_type: file.mimetype, // image/jpeg or image/png
            metadata
        });
        stream.pipe(writestream);
    }       
};
