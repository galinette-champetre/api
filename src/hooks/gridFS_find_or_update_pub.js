// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
    return async function gridFSFindOrUpdatePub(hook) {
        const gfs = hook.app.get('gfs');

        const data = await gfs.files.findOne({ filename: 'pub.jpg' });
        const { expiredAt } = hook.result.data[0];

        if (expiredAt && expiredAt <= Date.now()) {
            hook.result = {
                expiredAt,
                message: 'Pub expired !'
            }
            return hook
        }

        if (data) {
            return new Promise((resolve, reject) => {
                let buffer = [];
                const file = gfs.createReadStream({ filename: 'pub.jpg' })
                // on lit le stream
                file.on('data', function (chunk) {
                    buffer.push(chunk);
                })
                file.once('error', error => reject(error));
                // a la fin de la lecture
                file.on('end', function () {
                    const fbuf = Buffer.concat(buffer);
                    hook.result.buffer = fbuf.toString('base64');
                    console.log('Buffer', hook.result.buffer)

                    resolve(hook);
                });
            })
        }
    }
};
