// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return async function sendMail (hook) {
    const { file, text, gps } = hook.params;
    const emails = hook.data.contact.map(e => e.email)

    // const template = require('../mail/template');

    // const picture = {
    //   logoSmall: '../mail/images/logosmall.png'
    // };

    const email = {
      from: 'gil.felot@epitech.eu', // sender address
      to: emails, // list of receivers
      subject: 'Test Nodemailer', // Subject line
      text : `Description : ${text}\nGPS : https://maps.google.fr/maps?f=q&hl=fr&q=${gps.lat},${gps.lng}`, // plaintext body
      // html: template(picture), // html body
      attachments: [
        {
          filename: file.originalname,
          content: new Buffer(file.buffer, 'base64')
        }
      ]
    };

    await hook.app.service('mail').create(email)

    return hook;
  };
};
