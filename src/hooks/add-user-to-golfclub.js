// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return function addUserToGolfclub (hook) {
    const { user } = hook.params;
    return hook.app.service('users').patch(user._id, { $push: { golfClubIds: hook.result._id } })
      .then(result => hook);
  };
};
