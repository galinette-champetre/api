const users = require('./users/users.service.js');
const golfClubs = require('./golf-clubs/golf-clubs.service.js');
const issues = require('./issues/issues.service.js');
const mail = require('./mail/mail.service.js');
const pub = require('./pub/pub.service.js');
module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(users);
  app.configure(golfClubs);
  app.configure(issues);
  app.configure(mail);
  app.configure(pub);
};
