// Initializes the `mail` service on path `/mail`
const createService = require('./mail.class.js');
const hooks = require('./mail.hooks');
const filters = require('./mail.filters');

const nodemailer = require('nodemailer');
const transporter = nodemailer.createTransport({
  host: 'smtp.office365.com',
  port: 587,
  secure: false, // secure:true for port 465, secure:false for port 587
  auth: {
    user: 'gil.felot@epitech.eu',
    pass: 'MnQ1F-=s'
  }
});

transporter.verify(error => {
  if (error) {
    console.log(error);
  } else {
    console.log('Server is ready to take our messages');
  }
});


module.exports = function () {
  const app = this;

  const options = {
    name: 'mail',
    transporter
  };

  // Initialize our service with any options it requires
  app.use('/mail', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('mail');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
