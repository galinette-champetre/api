const { authenticate } = require('feathers-authentication').hooks;
const commonHooks = require('feathers-hooks-common');
const { restrictToOwner } = require('feathers-authentication-hooks');
const { disableMultiItemChange } = require('feathers-hooks-common');
// const { shallowPopulate } = require('feathers-shallow-populate');

const { hashPassword } = require('feathers-authentication-local').hooks;

const restrict = [
  authenticate('jwt'),
  restrictToOwner({
    idField: '_id',
    ownerField: '_id'
  })
];

// const options = {
//   include: {
//     service: 'golf-clubs',
//     nameAs: 'golfClub',
//     keyHere: 'golfClubIds',
//     keyThere: '_id'
//   }
// };

module.exports = {
  before: {
    all: [],
    find: [ authenticate('jwt') ],
    get: [ ...restrict],
    create: [ hashPassword() ],
    update: [...restrict, hashPassword(), disableMultiItemChange() ],
    patch: [...restrict, hashPassword(), disableMultiItemChange() ],
    remove: [...restrict, disableMultiItemChange() ]
  },

  after: {
    all: [
      commonHooks.when(
        hook => hook.params.provider,
        commonHooks.discard('password')
      )
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
