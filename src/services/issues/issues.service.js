// Initializes the `issues` service on path `/issues`
const createService = require('feathers-mongoose');
const moment = require('moment');
const createModel = require('../../models/issues.model');
const hooks = require('./issues.hooks');
const filters = require('./issues.filters');

const multer = require('multer');
const upload = multer();

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'issues',
    Model,
    paginate
  };

  // Initialize our service with any options it require.
  app.post('/issues', upload.single('uri'), (req, res, next) => {
    const mimeTypeArray = req.file.mimetype.split('/');
    const mimeType = mimeTypeArray[1];
    req.feathers.file = req.file;
    req.feathers.file.originalname = `Issue ${moment().format('LL')}.${mimeType}`;
    req.feathers.text = req.body.text;
    req.feathers.gps = req.body.gps;
    req.feathers.golfId = req.body.golfId;
    next();
  });

  app.use('/issues', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('issues');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
