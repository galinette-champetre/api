const { authenticate } = require('feathers-authentication').hooks;
const { disableMultiItemChange, iff, isNot, isProvider } = require('feathers-hooks-common');

const addUserToGolfclub = require('../../hooks/add-user-to-golfclub');

module.exports = {
  before: {
    all: [ iff(isNot(isProvider('internal')), authenticate('jwt')) ],
    find: [],
    get: [],
    
    create: [],
    update: [disableMultiItemChange()],
    patch: [disableMultiItemChange()],
    remove: [disableMultiItemChange()]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [addUserToGolfclub()],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
