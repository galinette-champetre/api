// Initializes the `pub` service on path `/pub`
const createService = require('feathers-mongoose');
const createModel = require('../../models/pub.model');
const hooks = require('./pub.hooks');
const filters = require('./pub.filters');
const multipartMiddleware = require('multer')();

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'pub',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/pub', 
    multipartMiddleware.single('uri'),
    function (req, res, next) {
      req.feathers.file = req.file;
      next();
    },
    createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('pub');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
