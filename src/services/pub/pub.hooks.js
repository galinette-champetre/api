const gridFSSavePub = require('../../hooks/gridFS_save_pub')
const gridFSFindOrUpdatePub = require('../../hooks/gridFS_find_or_update_pub')

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [gridFSSavePub()],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [gridFSFindOrUpdatePub()],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
