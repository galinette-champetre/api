// golf clubs-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const golfClubs = new Schema({
    name: { type: String, trim: true, index: true },
    address: {
      club: {
        number: { type: String, trim: true },
        street: { type: String, trim: true, lowercase: true },
        zip: { type: String, trim: true, lowercase: true },
        city: { type: String, trim: true, uppercase: true },
        country: { type: String, trim: true, uppercase: true },
        phone: { type: String, trim: true },
        email: { type: String, trim: true, lowercase: true },
        contactName: { type: String, trim: true }
      },
      bill: {
        number: { type: String, trim: true },
        street: { type: String, trim: true, lowercase: true },
        zip: { type: String, trim: true, lowercase: true },
        city: { type: String, trim: true, uppercase: true },
        country: { type: String, trim: true, uppercase: true },
        phone: { type: String, trim: true },
        email: { type: String, trim: true, lowercase: true }
      }
    },
    contact: [
      {
        name: { type: String, trim: true },
        lastname: { type: String, trim: true },
        email: { type: String, trim: true, lowercase: true },
        phone: { type: String, trim: true }
      }
    ],
    licence: {
      number: { type: String, trim: true },
      expired: Date
    },
    issueIds: [{
      type: mongooseClient.Schema.ObjectId,
      ref: 'issue'
    }],
    status: Boolean,
  });

  return mongooseClient.model('golfClubs', golfClubs);
};
