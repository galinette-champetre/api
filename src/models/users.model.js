// users-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const users = new mongooseClient.Schema({
  
    name: { type: String, trim: true },
    lastname: { type: String, trim: true },
    tel: { type: String, trim: true },
    email: { type: String, trim: true, required: true, index: true, unique: true },
    password: { type: String, required: true },
    address: { type: String, trim: true },
    golfClubIds: [{
      type: mongooseClient.Schema.ObjectId,
      ref: 'golfClub'
    }],
  
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
  });

  return mongooseClient.model('users', users);
};
