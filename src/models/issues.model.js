// issues-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const issues = new Schema({
    text: String,
    // img: String,
    gps: {lat: Number, lng: Number},
    golfId: {
      type: mongooseClient.Schema.ObjectId,
      ref: 'golfClubs'
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    done: { type: Boolean, default: false }
  });
  return mongooseClient.model('issues', issues);
};
