const mongoose = require('mongoose');
const Grid = require('gridfs-stream');

Grid.mongo = mongoose.mongo;

module.exports = function () {
  const app = this;

  mongoose.connect(app.get('mongodb'), {
    useMongoClient: true
  });
  const cnx = mongoose.connection;
  mongoose.Promise = global.Promise;

  cnx.once('open', () => {
    const gfs = Grid(cnx.db);
    app.set('gfs', gfs);
  });

  app.set('mongooseClient', mongoose);
};
